"""
Author: Alain Moré Maceda
Scraper de productos ERECTUS MX
"""
import csv
import json
import logging
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 10)
actions = ActionChains(driver)

added_products = []


def set_query_tag(connection):
    select = """
        ALTER SESSION SET QUERY_TAG = "global-ecommerce-data";
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(select)
    formatted_template = template.render()
    log.info(formatted_template)
    try:
        df_query_tag = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info(df_query_tag)
    except Exception as ex:
        log.error(ex)


def get_dom(url):
    """
    gets the DOM of the requested url using selenium NA
    """
    driver.get(url)


def do_smooth_scroll():
    """
    Smoothly scrolls the current page all the way to the bottom,
    giving time to results to load.
    """
    time.sleep(3)
    driver.execute_async_script(
        """
            count = 400;
            let callback = arguments[arguments.length - 1];
            t = setTimeout(function scrolldown(){
                console.log(count, t);
                window.scrollTo(0, count);
                if(count < (document.body.scrollHeight || document.documentElement.scrollHeight)){
                count+= 400;
                t = setTimeout(scrolldown, 1000);
                }else{
                callback((document.body.scrollHeight || document.documentElement.scrollHeight));
                }
            }, 1000);"""
    )


def cleanup_snowflake_tables(connection):
    continue_process = True
    log.info("Cloning backup data")
    clone = "CREATE OR REPLACE TABLE GLOBAL_ECOMMERCE.ERECTUS_PRODUCTS_BCK CLONE GLOBAL_ECOMMERCE.ERECTUS_PRODUCTS"
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(clone)
    formatted_template = template.render()
    log.info(formatted_template)
    try:
        df_clone = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Clone OK")
        log.info(df_clone)
    except Exception as ex:
        continue_process = False
        log.exception("Clone ERROR")

    if continue_process:
        log.info("Deleting old data")
        delete = "DELETE FROM GLOBAL_ECOMMERCE.ERECTUS_PRODUCTS"
        template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(delete)
        formatted_template = template.render()
        log.info(formatted_template)
        try:
            df_delete = pd.read_sql_query(con=connection, sql=formatted_template)
            log.info("Delete OK")
            log.info(df_delete)
        except Exception as ex:
            continue_process = False
            log.exception("Delete ERROR")

    return continue_process


def insert_product(connection, product):
    """
    Inserts current processed product into Snowflake
    """
    insert = """
        INSERT INTO GLOBAL_ECOMMERCE.ERECTUS_PRODUCTS 
            (SKU,NAME,CATEGORY,CURRENT_PRICE,PICTURES,ONLINE_AVAILABILITY,
                SUMMARY,DESCRIPTION,URL,SCRAPE_DATETIME)
        VALUES(
            '{{sku}}',
            '{{prod_name}}',
            '{{prod_category}}',
            {{price}},
            '{{pictures}}',            
            '{{status_1}}',
            '{{summary}}',
            '{{description}}',
            '{{prod_url}}',
            '{{scrape_datetime}}'  
        )     
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**product)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Insert OK")
        log.info(df_insert)
    except Exception as ex:
        log.info("Insert ERROR")
        log.error(ex)


def get_prod_details_from_listing(prod_category, result):
    """
    Obtiene la url de los productos desplegados desde el
    listing de categoría.
    """
    prod_url = ""

    prod_url_tag = result.find("a", {"class": "product-image-link"})
    if prod_url_tag:
        prod_url = prod_url_tag["href"]
    else:
        log.info("No se pudo obtener prod_url")

    product_info = {"prod_category": prod_category, "prod_url": prod_url}
    return product_info


def start_scraping(url, connection):
    """
    Scraping main function, orchestrates full process
    """
    log.info("")
    log.info(">>>>>>>> CATEGORIA: %s", url)
    log.info("")
    get_dom(url)
    do_smooth_scroll()
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")
    prod_category = cat_soup.find("span", {"class": "breadcrumb-last"}).text.strip()
    results = cat_soup.find_all("div", {"class": "product-element-top"})
    log.info("Categorias {}".format(prod_category))
    log.info("Productos {}".format(len(results)))

    # Obtener los detalles de cada producto, desde la pagina de resultados
    products = []
    i = 0
    for result in results:
        product_info = []
        log.info(">>>>>>>>>>>>>>>>>>>>")
        log.info("Producto: %i", i)
        product_info = get_prod_details_from_listing(prod_category, result)
        if product_info["prod_url"] not in added_products:
            products.append(product_info)
            log.info("Nuevo producto agregado: ")
            log.info(product_info)
            added_products.append(product_info["prod_url"])
        else:
            log.info("Producto duplicado: ")
            log.info(product_info)

        i = i + 1

    # Obtener detalles desde la página del producto
    for product in products:
        get_dom(product["prod_url"])
        time.sleep(1)
        prod_soup = BeautifulSoup(driver.page_source, "html.parser")

        # NAME
        try:
            prod_name = (
                prod_soup.find("h1", {"class": "product_title wd-entities-title"})
            ).text.strip()
            log.info(prod_name)
            product["prod_name"] = prod_name
        except Exception as ex:
            log.info("No se pudo obtener nombre")
            log.error(ex)

        # PICTURES
        pictures = []
        picture_tags = prod_soup.find_all("div", {"class": "product-image-wrap"})
        if picture_tags:
            max_pictures = 0
            for pic_tag in picture_tags:
                pic_tmp = pic_tag.find("a")
                if pic_tmp:
                    pictures.append(pic_tmp["href"])
                    max_pictures = max_pictures + 1
                if max_pictures >= 3:
                    break

            pics_json = json.dumps(pictures)
            product["pictures"] = pics_json
        else:
            log.info("No se pudieron obtener imagenes")

        # SKU
        sku = ""
        sku_tag = prod_soup.find("span", {"class": "sku"})
        if sku_tag:
            sku = sku_tag.text.strip()
            product["sku"] = sku
        else:
            log.info("No se pudo obtener sku")

        # Online availability
        try:
            status_1 = prod_soup.find(
                "p", {"class": "stock in-stock in_stock_color woo-custom-stock-status"}
            ).text.strip()
            product["status_1"] = status_1
        except Exception as ex:
            log.info("No se pudo obtener online stock")
            log.error(ex)

        # Price
        try:
            price = prod_soup.find("p", {"class": "price"}).text.strip()
            product["price"] = float(price.replace("$", "").replace(",", ""))
        except Exception as ex:
            log.info("No se pudo obtener price")
            log.error(ex)

        # SUMMARY
        summary = ""
        summary_tag = prod_soup.find(
            "div", {"class": "woocommerce-product-details__short-description"}
        )
        if summary_tag:
            summary = summary_tag.text.strip().replace("'", "")
            product["summary"] = summary
        else:
            log.info("No se pudo obtener summary")

        # DESCRIPTION
        description = ""
        description_tag = prod_soup.find("div", {"id": "tab-description"})
        if description_tag:
            description = description_tag.text.strip().replace("'", "")
            product["description"] = description
        else:
            log.info("No se pudo obtener description")

        product["scrape_datetime"] = datetime.now()

        insert_product(connection, product)
        log.info(product)
        log.info("")
        log.info("")
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        log.info("")
        log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS START!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "ECOMMERCE",
        "role": "GLOBAL_ECOMMERCE_WRITE_ROLE",
        "database": "fivetran",
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    try:
        set_query_tag(connection)
        continue_process = cleanup_snowflake_tables(connection)
        continue_process = True
        if continue_process:
            with open("categories.csv", "r") as csvfile:
                csvreader = csv.DictReader(csvfile)
                for row in csvreader:
                    cat_url = row["category_url"]
                    start_scraping(cat_url, connection)

            driver.close()
            print("PROCESS FINISHED OK!")
        else:
            print("Error al respaldar y/o borrar data anterior")
    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
